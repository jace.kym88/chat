import "babel-polyfill";
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx';
import {initialMessages, mockChats1, mockChats2} from './mockData/exampleChatData.js'; 

/* ReactDOM.render(<App messages={mockChats2}/>, document.getElementById('app'));*/
ReactDOM.render(<App />, document.getElementById('app'));
